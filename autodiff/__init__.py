from autodiff.autotensor import autoTensor, Node
from autodiff.functional import Dpout, Flatten2d, Add, MatMul, Multiply, Negate, Substract, Power, Divide, Sum, tanh, sigmoid, relu, Exp