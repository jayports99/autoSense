# autoSense

A Minimalist Deep learning library

We introduce autoTensor, a boxed pytorch tensor capable of backpropagation using automatic differntiation.

## autoDiff: Reverse Accumulation Mode Automatic Differntiation


### Working documentation

Every function is an autoTensor that has a backpropagation channel to compute the gradients with respect to dependent autoTensor.

![Example](https://github.com/jay1999ke/autodiff/raw/master/autodiff.jpeg)


#### TODO : Add documentation


